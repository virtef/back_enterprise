from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin

class Company(models.Model):
    name = models.CharField(max_length=50)
    direccion = models.CharField(max_length=100)
    phone = models.CharField(max_length=20)
    domain = models.CharField(max_length=50)

    def __str__(self):
        return self.name
    
    #  def get_full_name(self):
    #      return self.full_name


class CustomUserManager(BaseUserManager):
    def create_user(self, email, password=None, **extra_fields):
        if not email:
            raise ValueError('El correo electrónico es obligatorio')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        return self.create_user(email, password, **extra_fields)

    def authenticate(self, request, email=None, password=None, **kwargs):
        if email is None:
            email = kwargs.get('email')
        if email is None or password is None:
            return
        try:
            user = self.get(email=email)
        except self.model.DoesNotExist:
            return
        if user.check_password(password):
            return user

class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    date_joined = models.DateTimeField(auto_now_add=True)
    company = models.OneToOneField(Company, on_delete=models.CASCADE,null=True, default=None)

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name']


# class User(AbstractBaseUser, PermissionsMixin):

#     GENDER_CHOICES = (
#         ('M', 'Masculino'),
#         ('F', 'Femenino'),
#         ('O', 'Otros'),
#     )

#     email = models.EmailField(unique=True)
#     full_name = models.CharField('Nombres', max_length=100)
#     genero = models.CharField(
#         max_length=1, 
#         choices=GENDER_CHOICES, 
#         blank=True
#     )
#     date_birth = models.DateField(
#         'Fecha de nacimiento', 
#         blank=True,
#         null=True
#     )
#     #
#     is_staff = models.BooleanField(default=False)
#     is_active = models.BooleanField(default=False)

#     USERNAME_FIELD = 'email'

#     REQUIRED_FIELDS = ['full_name']

#     objects = CustomUserManager()

#     def get_short_name(self):
#         return self.email
    
#     def get_full_name(self):
#         return self.full_name