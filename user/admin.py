from django.contrib import admin
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from user.models import Company, CustomUser

admin.site.register(ContentType)
admin.site.register(Permission)
admin.site.register(CustomUser)
admin.site.register(Company)