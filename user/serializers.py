from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from user.models import CustomUser


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('id', 'name')

class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    pass

class ContentTypeSerilizer(serializers.ModelSerializer):
    class Meta:
        model = ContentType
        fields = '__all__'

class PermissionSerializer(serializers.ModelSerializer):
    content_type = ContentTypeSerilizer
    class Meta:
        model = Permission
        fields = ('id', 'name', 'codename', 'content_type')
        
class UserSerializer(serializers.ModelSerializer):
    user_permissions = PermissionSerializer(many=True, required=False, allow_null=True, default=None)
    groups = GroupSerializer(many=True, required=False, allow_null=True, default=None)
    last_name = serializers.CharField(required=False, allow_blank=True, max_length=50)
    class Meta:
        model = CustomUser
        fields = ('email', 'first_name', 'last_name', 'groups', 'is_superuser', 'last_login', 'date_joined', 'is_staff', 'is_active', 'id', 'user_permissions')
    
    def create(self, validated_data):
        user = CustomUser.objects.create_user(**validated_data)
        return user